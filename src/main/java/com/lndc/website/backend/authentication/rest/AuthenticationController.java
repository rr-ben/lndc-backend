package com.lndc.website.backend.authentication.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lndc.website.backend.ApiController;
import com.lndc.website.backend.users.services.RoleService;
import com.lndc.website.backend.users.services.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class AuthenticationController {

  @Autowired
  AuthenticationManager authenticationManager;

  @Autowired
  UserService userService;

  @Autowired
  RoleService roleService;

  @Value("${lndc.app.corsPolicy}")
  private String corsPolicy;

  @PostMapping(ApiController.API_LOGIN)
  public ResponseEntity<String> authenticateUser() {
    return new ResponseEntity<String>("{ \"message\": \"PUBLIC OK\" }", HttpStatus.OK);
  }

  @PostMapping(ApiController.API_LOGOUT)
  public ResponseEntity<String> logoutUser() {
    return new ResponseEntity<String>("{ \"message\": \"PRIVATE OK\" }", HttpStatus.OK);
  }
}
