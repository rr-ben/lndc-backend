CREATE DATABASE IF NOT EXISTS lndc;
USE lndc;

CREATE TABLE IF NOT EXISTS role (
    id INT NOT NULL AUTO_INCREMENT,
    uuid VARCHAR(255) NOT NULL,
    name VARCHAR(255) NOT NULL,
    description  VARCHAR(255) NOT NULL,
    created_date DATE NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (uuid),
    UNIQUE (name)
) ENGINE=InnoDB CHARSET=utf8;

CREATE TABLE IF NOT EXISTS user (
    id INT NOT NULL AUTO_INCREMENT,
    uuid VARCHAR(255) NOT NULL,
    password_hash VARCHAR(255) NOT NULL,
    role_id INT NOT NULL,
    username VARCHAR(255) NOT NULL,
    first_name VARCHAR(255),
    last_name VARCHAR(255),
    email VARCHAR(255) NOT NULL,
    phone VARCHAR(255),
    university VARCHAR(255),
    city VARCHAR(255),
    hobbies VARCHAR(255),
    created_date DATE NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (uuid),
    UNIQUE (username),
    UNIQUE (email),
    FOREIGN KEY (role_id) REFERENCES role(id)
) ENGINE=InnoDB CHARSET=utf8;
